% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/Data Integration.R
\name{Data_Integration}
\alias{Data_Integration}
\title{Combine Data together}
\usage{
Data_Integration(update_data = FALSE)
}
\arguments{
\item{update_data}{Do you wish to update the internal data? (Will take the function to run a bit longer).}
}
\value{

}
\description{
Use this function to get all related data in the MagnifyingGlass. This function will return the combined summary
level data, as well as related dataframes in a list. You can choose to update the data by setting update_data to TRUE.
This will update the data in the function and return the updated data in the list. It will not update the data returned
by the package. If you want any documentation on the datasets, just search them up in the package documentation.
}
\examples{
\dontrun{
Data_Integration()
}
}
