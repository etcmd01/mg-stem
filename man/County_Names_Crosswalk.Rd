% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/Documenting Datasets.R
\docType{data}
\name{County_Names_Crosswalk}
\alias{County_Names_Crosswalk}
\title{County Names Crosswalk}
\format{
An object of class \code{tbl_df} (inherits from \code{tbl}, \code{data.frame}) with 3272 rows and 6 columns.
}
\usage{
County_Names_Crosswalk
}
\description{
Data for translating fips code to county name.
}
\author{
Edward LaHaye \email{elahaye@etcmd.com}
}
\keyword{datasets}
