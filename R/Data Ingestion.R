# So lets just start off with a simple function to save the County crosswalk
# 

#' Read in the zip to count crosswalk
#'
#' @param internal Will this be used as an internal function or not? If set false, returns object as DF.
#' @param path The file path to the file you wish to use as a crosswalk.
#'
#' @description This is an internal function just for processing the Zip code crosswalk from hud user.
#' Link to it here: https://www.huduser.gov/portal/datasets/usps_crosswalk.html#data
#' @return
#' @importFrom usethis use_data
#' @importFrom readxl read_excel
#' @examples
#' \dontrun{
#' save_county_zip_crosswalk()
#' }
save_county_zip_crosswalk <- function(path = "inst/extdata/County data/ZIP_COUNTY_032021.xlsx", internal = TRUE) {
  
  county <- read_excel(path)
  #only need the data that I reall need
  county <- county[, 1:4]
  
  if (internal == TRUE) {
    usethis::use_data(county, overwrite = T)
  } else {
    return(county)
  }
  

}


## Ok lets get SBIR data in (Gonna use the bulk monthly data as opposed to API)
## 

#'  Save SBIR data from the website
#'
#' @param internal Will this be used as an internal function or not? If set false, returns object as DF.
#'
#' @return
#' @importFrom  here here
#' @importFrom usethis use_data
#' @importFrom readr read_csv
#' @import dplyr
#' @importFrom stringr str_extract
#' @examples
#' \dontrun{
#' save_sbir_data()
#' }
save_SBIR_data <- function(internal = TRUE) {
  
  url <- "https://data.www.sbir.gov/awarddatapublic/award_data.csv"
  #lets create a directory beforehand to handle the file
  # dir.create(here("awards_data_download"))
  download.file(url, destfile = here("awards_data.csv"))
  all_awards <- read_csv(here("awards_data.csv"))
  # Now lets process and add in other data on top of it
  data(acs)
  
  county_codes <- acs %>% 
    select(state, county, NAME) %>% 
    distinct() %>% 
    mutate(combined_code = paste0(state, county))
  
  all_awards <- all_awards %>% 
    mutate(Zip = str_extract(pattern = "^[:digit:]....", string = Zip),
           award_id = row_number()) %>% 
    left_join(county[, c("ZIP","COUNTY")],by = c("Zip" = "ZIP")) %>% 
    left_join(county_codes, by = c("COUNTY" = "combined_code")) %>% 
    distinct() %>% 
    group_by(award_id) %>% 
    arrange(county) %>% 
    slice(1) %>% 
    ungroup()
  
  if (internal == TRUE) {
    usethis::use_data(all_awards, overwrite = T)
  } else {
    return(all_awards)
  }
  
  #delete the residual file
  unlink(here("awards_data.csv"), recursive = T)
}


#' Load the Business owners data
#'
#' @param path path to the csv file
#' @param internal Will this be used as an internal function or not? If set false, returns object as DF.
#'
#' @return
#' @importFrom readr read_csv
#' @importFrom usethis use_data
#'
#' @examples
#' \dontrun{
#' save_business_owners()
#' }
#' 
save_business_owners <- function(path = "inst/extdata/County data/Bussiness Owner Features Detailed to the Point of Almost Unreadible.csv", 
                                 internal = TRUE) {

  Business_Owners_data <- read_csv(path, 
                                   skip = 1)
  if (internal == TRUE) {
    usethis::use_data(Business_Owners_data, overwrite = TRUE)
  } else {
    return(Business_Owners_data)
  }
  
}


#' Ingest Crosswalk for counties names
#'
#' @param internal Will this be used as an internal function or not? If set false, returns object as DF.
#'
#' @return
#' @importFrom readr read_csv
#' @importFrom usethis use_data
#' @examples
#' \dontrun{
#' save_County_Names_Crosswalk()
#' }
save_County_Names_Crosswalk <- function(internal = TRUE) {
  
  County_Names_Crosswalk <- read_excel("inst/extdata/County data/FY2018-CMS-1677-FR-CBSA-Crosswalk/FY 2018 FR Cty CBSA Xwalk and CBSA Con Cty.xlsx", 
                                       sheet = "Crosswalk")
  
  if (internal == TRUE) {
    usethis::use_data(County_Names_Crosswalk, overwrite = TRUE)
  } else {
    return(County_Names_Crosswalk)
  }
  

}




#' Save and Update Census Data Sources
#'
#' @param api_key API key for the US Census(Get one here https://www.census.gov/data/developers/guidance/api-user-guide.html)
#' @param internal Will this be used as an internal function or not? If set false, returns object as DF.
#'
#' @description 
#' This function gives us the the fields:
#' 1. Median Household Income
#' 2. College Degree %
#' 3. Stem Degree %
#' 4. Women Owned Small Business
#' 5. Veteran Owned businesses
#' 6. SDVOSB
#' 7. Minority Owned Small Businesses
#'  These come from 2 sources, the 2019 American Community Survey 5 Year Estimates and the 2017 American Business Survey.
#'  Both Datasets are set for the County level, and are directly connected via API to the US Census. This function updates
#'  the acs and abscs data within the package.
#' @export
#' @import dplyr
#' @import censusapi
#' @examples
#' \dontrun{
#' save_census_data()
#' }
save_census_data <- function(api_key = "9628cebbfb525f5b3e962b767201824d62a6850b",
                             internal = TRUE) {

  # Metrics needed from census api
  # 1. Median Household Income
  # 2. College Degree %
  # 3. Stem Degree %
  # 4. Women Owned Small Business
  # 5. Veteran Owned businesses
  # 6. SDVOSB
  # 7. Minority Owned Small Businesses
  #
  # Obvisoualy these all have to have the county level
  # set api key
  
  Sys.setenv(CENSUS_KEY = api_key)
  
  # Using the ACS 5 year estimates from 2019
  
  # Getting from ACS/ACS1 == the 2019 American Community Survey
  # 1: B19013_001E =  Median Houshold income
  # 2:B29002_007E = College Degree %
  # 3: C15010_002E = Science and Engineering Degrees
  # 4. B01003_001E = total population
  
  # acs_metadata <- listCensusMetadata(name = "acs/acs5", vintage = "2019")
  acs <-
    getCensus(
      name = "acs/acs5",
      vintage = "2019",
      vars = c(
        "NAME",
        "B19013_001E",
        "B29002_007E",
        "B01003_001E",
        "C15010_002E"
      ),
      region = "county"
    )
  
  # Lets just rename the data for easy analysis
  acs <- acs %>%
    rename(
      "Med_House_Income" = B19013_001E,
      "POP_Bachelors_degree" = B29002_007E,
      "POP_Science_Engineering_Bachelors" = C15010_002E,
      "Total_POP" = B01003_001E
    ) %>%
    mutate(
      Med_House_Income = as.numeric(Med_House_Income),
      POP_Bachelors_degree = as.numeric(POP_Bachelors_degree),
      POP_Science_Engineering_Bachelors = as.numeric(POP_Science_Engineering_Bachelors)
    )
  
  ##lets go with ABSCS in 2017, as they have county level data
  # 1. Total Veteran owned businesses per county
  # 2. Total Women Owned Businesses per county
  # 3. Total minority Owned Businesses per county
  # 4. Total Businesses per county
  
  # abscs_metadata <- listCensusMetadata(name = "abscs", vintage = "2017")
  abscs_variables <-
    c(
      "VET_GROUP",
      "VET_GROUP_LABEL",
      "SEX",
      "SEX_LABEL",
      "RACE_GROUP",
      "RACE_GROUP_LABEL",
      "FIRMPDEMP",
      "EMP",
      "NAME"
    )
  abscs <-
    getCensus(
      name = "abscs",
      vintage = "2017",
      vars = abscs_variables,
      region = "COUNTY"
    )
  
  # I want to remove the columns that arn't really needed
  abscs <- abscs %>%
    select(-SEX, -RACE_GROUP, -VET_GROUP)

  ## Summarize the ABSCs to be in a tidy format
  
  # 1. Total Veteran owned business (with pct ratio)
  
  abscs <- abscs %>%
    mutate(FIRMPDEMP = as.numeric(FIRMPDEMP)) %>%
    group_by(state, county, NAME) %>%
    summarize(
      Veteran_Business = sum(FIRMPDEMP[VET_GROUP_LABEL == "Veteran"], na.rm = TRUE),
      Women_Owned_Business = sum(FIRMPDEMP[SEX_LABEL == "Female"], na.rm = TRUE),
      Minority_Owned_Business = sum(FIRMPDEMP[RACE_GROUP_LABEL == "Minority"], na.rm = TRUE),
      Total_Firms = sum(FIRMPDEMP[VET_GROUP_LABEL == "Total" &
                                    SEX_LABEL == "Total" &
                                    RACE_GROUP_LABEL == "Total"], na.rm = TRUE)
    )
 
  
  if (internal == TRUE) {
    usethis::use_data(acs, overwrite = TRUE)
    usethis::use_data(abscs, overwrite = TRUE)
  } else {
    censuslist <- list(acs, abscs)
    return(censuslist)
  }
  
}

