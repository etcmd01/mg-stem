# The purpose of this file is to pull together all the data that we need
# I think we should use targets for reproducibilitiy

# library(MGNavyStem)
# library(tidyverse)
# library(DataExplorer)
# Lets combine census data

#' Combine Data together
#' @description 
#' Use this function to get all related data in the MagnifyingGlass. This function will return the combined summary
#' level data, as well as related dataframes in a list. You can choose to update the data by setting update_data to TRUE.
#' This will update the data in the function and return the updated data in the list. It will not update the data returned
#' by the package. If you want any documentation on the datasets, just search them up in the package documentation.
#'
#' @param update_data Do you wish to update the internal data? (Will take the function to run a bit longer).
#'
#' @return
#' @export
#' @import dplyr
#' @import stringr
#' 
#'
#' @examples
#' \dontrun{
#' Data_Integration()
#' }
Data_Integration <- function(update_data = FALSE) {
  # Update internal data if needed
  if (update_data == TRUE) {
    
    datalist <- UpdateData(internal = FALSE)
    acs <- datalist$acs
    abscs <- datalist$abscs
    all_awards <- datalist$SBIR_awards
    county <- datalist$zip_crosswalk
  } 
  
  # data("acs")
  # data("abscs")
  Census_Data <- acs %>% 
    inner_join(abscs, by= c("NAME")) %>% 
    select(-state.y, -county.y) %>% 
    rename("state" = state.x,
           "county" = county.x)
  
  #lets attatch a county to each award
  # data("all_awards")
  # data("county")
  
  
  
  awards_summarized<- all_awards %>% 
    group_by(COUNTY ,NAME) %>% 
    summarise(SBIR_Awards = n(),
              SBIR_Award_Money = sum(`Award Amount`, na.rm = TRUE)) %>% 
    ungroup() %>% 
    mutate(SBIR_rank = dense_rank(desc(SBIR_Award_Money)))
    
  
  # join together
  
  combined_data <- Census_Data %>% 
    left_join(awards_summarized, by = c("NAME")) 
  # return(combined_data)
  
  # Lets turn it into a list of DF for consumption
  
   datalist <- list("combined_data" = combined_data, 
                    "abscs" = abscs, 
                    "acs" = acs, 
                    "all_awards" = all_awards)
   return(datalist)

}



##################################### Other THINGS #####################################################################

# library(MGNavyStem)
# data("acs")
# data("abscs")
# Census_Data <- acs %>% 
#   inner_join(abscs, by= c("NAME")) %>% 
#   select(-state.y, -county.y) %>% 
#   rename("state" = state.x,
#          "county" = county.x)
# 
# #lets attatch a county to each award
# data("all_awards")
# data("county")
# 
# county_codes<- Census_Data %>% 
#   select(state, county, NAME) %>% 
#   distinct() %>% 
#   mutate(combined_code = paste0(state, county))
# 
# all_awards2 <- all_awards %>% 
#   mutate(Zip = str_extract(pattern = "^[:digit:]....", string = Zip),
#          award_id = row_number()) %>% 
#   left_join(county[, c("ZIP","COUNTY")],by = c("Zip" = "ZIP")) %>% 
#   left_join(county_codes, by = c("COUNTY" = "combined_code")) %>% 
#   distinct() %>% 
#   group_by(award_id) %>% 
#   arrange(county) %>% 
#   slice(1) 
# plot_missing(all_awards2)
# 
# awards_summarized<- all_awards2 %>% 
#   group_by(COUNTY ,NAME) %>% 
#   summarise(SBIR_Awards = n(),
#             SBIR_Award_Money = sum(`Award Amount`, na.rm = TRUE)) %>% 
#   ungroup() %>% 
#   mutate(SBIR_rank = dense_rank(desc(SBIR_Award_Money)))
# 
# 
# # join together
# 
# combined_data <- Census_Data %>% 
#   left_join(awards_summarized, by = c("NAME")) 
# return(combined_data)


# just a quick visual for the SBIR awards

# # installed urbanmapr for easy stuff
# library(urbnmapr)
# 
# awards_summarized_map <- full_join(awards_summarized, counties, by = c("COUNTY" = "county_fips"))
# 
# awards_summarized_map %>% 
#   ggplot(aes(long, lat, group = group, fill = SBIR_Award_Money)) +
#   geom_polygon(color = NA) +
#   coord_map(projection = "albers", lat0 = 39, lat1 = 45) +
#   labs(fill = "SBIR Award Money")


# ################DeadStuff####################################
# 
# I think we should try to geocode some stuff so I'm gonna put this on pause
# library(tidyverse)
# library(censusxy)
# library(doParallel)
# library(foreach)
# 
# #lets prep the awards data
# 
# all_awards <- all_awards %>% mutate(award_id = row_number())
# 
# # awards_for_geocoding <- all_awards %>%
# #   select(award_id, Address1, City,State,Zip)
# time1 <- Sys.time()
# all_awards2 <- censusxy::cxy_geocode(.data = all_awards,
#                       id = "award_id",
#                       street = "Address1",city = "City", state = "State", zip = "Zip", return = "geographies", vintage = "2020", benchmark = "2020", parallel = 4)
# time2 <- Sys.time()
# time2-time1