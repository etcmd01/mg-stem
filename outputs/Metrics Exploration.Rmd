---
title: "Metric Exploration"
author: "Edward 'Bingo' La Haye"
output: html_notebook
---

## Intro

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. We will be using this document to explore the metrics that we want to track and how we want to format it.

```{r}
## Libraries


library(tidyverse)
library(DataExplorer)
library(DT)
library(silgelib)
library(ggthemes)

## Functions and underlying data

load(file = "data/full_set.rda")
theme_set(theme_clean())

```

## Lets visualize it

```{r}
glimpse(full_set)
full_set <- full_set %>% 
  ungroup() %>% 
  filter(is.na(`CBSA Title`) == F) %>% 
  mutate(across(.cols = c(`African American Pop`,`PCT African American Pop`,`Hispanic Pop`,`PCT Hispanic Pop`,`American Indian or Alaskan Native Pop`,`PCT American Indian or Alaskan Native Pop`,`PCT African American High School or Higher`,`PCT African American Bachelors Degree or higher`,`Total Pop over 25 with Bachelor degree or higher`,`Total Pop over 25 with Science & Engineering or higher`,`PCT over 25 with Science & Engineering or higher`,`Total POP Under 125 percent of poverty level`), ~ as.numeric(.)))
```

```{r fig.height=6, fig.width=8}
DataExplorer::plot_missing(full_set)
DataExplorer::plot_histogram(full_set)
DataExplorer::plot_qq(full_set)
```


Lets do a boxplot to see whats going on 

```{r fig.height=10, fig.width=17, dpi=400}
#just gonna take the numeric columns
full_set %>% 
  pivot_longer(cols =`African American Pop`:`Total POP Under 125 percent of poverty level`,names_to = "stat", values_to = "value") %>% 
  ggplot() +
  geom_boxplot(aes(y = value), col = "blue") +
  facet_wrap(~stat, scales = "free_y", nrow = 2)+
  labs(y = NULL, color = NULL, fill = NULL) +
  theme(strip.text = element_text(size = 7))
  
```

Quick few observations:

-   Over 25 with science an engineering is the closest to a normal distribution

-   Most demographics based columns are skewed to the right, with the PCT columns being more normal than total pop

    -   This means we might lean towards the PCT columns for analysis

## Lets modify it

### Try Number 1
Ok so we want to rate the continuous variables in a high medium low approach. This way we can get jut the right amount of information for each city.

We first need to define a function that we can apply to each census variable, so that we can get high/medium/low. Some of the columns have text based symbols in them and I'll have to remove them and convert from NA to 0. The below code converts all relevant columns to numeric, then assigns low, medium, and high based on relative to standard deviation from the mean.

The logic is as follows:

-   If it is greater than or equal to one standard deviation above the mean it is "High"

-   If it is less than or equal to one standard deviation below the mean it is "Low"

-   If it is missing a number altogether is is "Low"

-   If it is less than one standard deviation above the mean and greater than one standard deviation below the mean, then its is "Medium".

It is represented in the code like so ( "." refers to the variable):

```{r eval=FALSE, include=TRUE}
(. <= (mean(., na.rm = T) - sd(., na.rm = T)) ~ "Low",
                 . >= (mean(., na.rm = T) + sd(., na.rm = T)) ~ "High",
                 . > (mean(., na.rm = T) - sd(., na.rm = T)) & 
                   . < (mean(., na.rm = T) + sd(., na.rm = T)) ~ "Medium",
                 is.na(.) ~ "Low")
```


Now lets run it!

```{r}

full_set2 <- full_set %>% 
  ungroup() %>% 
  filter(is.na(`CBSA Title`) == F)  %>% 
  mutate(
    across(
      .cols = c(`African American Pop`,`PCT African American Pop`,`Hispanic Pop`,`PCT Hispanic Pop`,`American Indian or Alaskan Native Pop`,`PCT American Indian or Alaskan Native Pop`,`PCT African American High School or Higher`,`PCT African American Bachelors Degree or higher`,`Total Pop over 25 with Bachelor degree or higher`,`Total Pop over 25 with Science & Engineering or higher`,`PCT over 25 with Science & Engineering or higher`,`Total POP Under 125 percent of poverty level`), .fns = 
       ~ case_when(. <= (mean(., na.rm = T) - sd(., na.rm = T)) ~ "Low",
                 . >= (mean(., na.rm = T) + sd(., na.rm = T)) ~ "High",
                 . > (mean(., na.rm = T) - sd(., na.rm = T)) & 
                   . < (mean(., na.rm = T) + sd(., na.rm = T)) ~ "Medium",
                 is.na(.) ~ "Low"))) %>% 
  mutate(across(.cols = c(`African American Pop`,`PCT African American Pop`,`Hispanic Pop`,`PCT Hispanic Pop`,`American Indian or Alaskan Native Pop`,`PCT American Indian or Alaskan Native Pop`,`PCT African American High School or Higher`,`PCT African American Bachelors Degree or higher`,`Total Pop over 25 with Bachelor degree or higher`,`Total Pop over 25 with Science & Engineering or higher`,`PCT over 25 with Science & Engineering or higher`,`Total POP Under 125 percent of poverty level`), 
                ~ factor(., levels = c("Low", "Medium", "High"),ordered = T)))
```

Now lets see if this classification method worked. Lets do a quick visual.

```{r fig.height=12, fig.width=15, dpi=300}
DataExplorer::plot_bar(full_set2, ggtheme = theme_clean())
DataExplorer::plot_histogram(full_set, ggtheme = theme_clean())
```

We can see that most fall in the medium category, which I think reflects how spread out each of these distributions were. We had a large standard deviation, which might have caused this. We could remedy this by halving the standard deviation or reducing it in some way. I'm tempted to use a clustering approach. In general we are gonna want to use PCT based columns because they are more normally distributed.


### Try Number 2

Lets first just measure by PCT variables as from before they are more normally distributed.

Using the same formula for classification but this time we are dividing the Standard deviation by 1.6. This will narrow the amount of observations that are medium.(Should give us more lows and highs).

```{r}
#selecting the PCT columns
full_set_long <- full_set %>% 
  select(c(CBSA, `CBSA Title`), c(contains("PCT"))) %>% 
  pivot_longer(cols =contains("PCT"),names_to = "stat", values_to = "value") 

full_set3_long <- full_set %>% 
  select(c(CBSA, `CBSA Title`), c(contains("PCT"))) %>% 
  mutate(across(.cols = contains("PCT"), ~ case_when(. <= (mean(., na.rm = T) - sd(., na.rm = T)/1.6) ~ "Low",
                 . >= (mean(., na.rm = T) + sd(., na.rm = T)/1.6) ~ "High",
                 . > (mean(., na.rm = T) - sd(., na.rm = T)/1.6) & 
                   . < (mean(., na.rm = T) + sd(., na.rm = T)/1.6) ~ "Medium",
                 is.na(.) ~ "Low"))) %>% 
  mutate(across(.cols = contains("PCT"), ~ factor(., levels = c("Low", "Medium", "High"),ordered = T))) %>% 
  pivot_longer(cols =contains("PCT"),names_to = "stat", values_to = "value") 

full_set3 <- full_set %>% 
  select(c(CBSA, `CBSA Title`), c(contains("PCT"))) %>% 
  mutate(across(.cols = contains("PCT"), ~ case_when(. <= (mean(., na.rm = T) - sd(., na.rm = T)/1.6) ~ "Low",
                 . >= (mean(., na.rm = T) + sd(., na.rm = T)/1.6) ~ "High",
                 . > (mean(., na.rm = T) - sd(., na.rm = T)/1.6) & 
                   . < (mean(., na.rm = T) + sd(., na.rm = T)/1.6) ~ "Medium",
                 is.na(.) ~ "Low"))) %>% 
  mutate(across(.cols = contains("PCT"), ~ factor(., levels = c("Low", "Medium", "High"),ordered = T)))
```

Ok lets join them together and plot. We want to make sure we are classifying them in a better way. 

```{r fig.height=7, fig.width=10, r,dpi=300}
full_set3_long %>% 
  left_join(full_set_long, by = c("CBSA", "CBSA Title", "stat")) %>% 
  ggplot() +
    geom_histogram(aes(x = value.y, fill = value.x), bins = 35) +
    facet_wrap(~stat, scales = "free_y", nrow = 2)+
    labs(y = NULL, color = NULL, fill = NULL) +
    theme(strip.text = element_text(size = 7))

full_set3_long %>% 
  left_join(full_set_long, by = c("CBSA", "CBSA Title", "stat")) %>% 
  ggplot() +
  geom_col(aes(x = value.x, y = value.y, fill = value.x)) +
  facet_wrap(~stat, scales = "free_y", nrow = 2) +
  labs(y = NULL, color = NULL, fill = NULL) +
  theme(strip.text = element_text(size = 7))
```


Ok this finally looks decent, so its time to summarize the other data and put it together.

### Summarise other data

We want to add Yes or no for the data on the non census data.

```{r}
full_set4 <- full_set %>% 
  select(CBSA, `Warfare Centers`:HSI_count) %>% 
  left_join(full_set3, by = c("CBSA")) %>% 
  mutate(across(.cols = c(`Warfare Centers`, `DoD Labs`, `Strive Together`, `HBCU_count`, `HSI_count`), 
                ~ case_when(. >= 1 ~ "x",
                            . == 0 ~ ""))) %>% 
  relocate(`CBSA Title`, .before = `Warfare Centers`)
```

### Integrate legacy selection data

After that we just need to see if we can preserve the decisions made in the last meeting. This will include some columns that are partially subjective. Lets import it and clean where we can.

```{r}
library(readxl)
legacy <- read_excel("inst/extdata/City Selection Broken Out by Intrigue - 4 MAR 2021 - Matt.xlsx", 
    skip = 1)
colnames(legacy)[1] <- "City"

# clean up Na
legacy <- legacy %>% 
  mutate(across(.cols = where(is.character), ~ replace_na(., ""))) %>% 
  select(-`ABET HSI`, -`ABET MSI`)

```

lets see if we can join this up 1 to 1

```{r}
legacy %>% 
  inner_join(full_set4, by = c("City" = "CBSA Title"))
```
We get only 11 rows back, seems like they tampered with some of the names. I might have to do this manually if needed. I'm gonna try to fuzzy join just as a last measure.


```{r}
# libraries
library(stringdist)

#  code

legacy %>% 
  stringdist_inner_join(full_set4, by = c("City" = "CBSA Title"))

```
Ok seems like I'll just match in manually for now.




# Write out

```{r}
library(openxlsx)

full_set %>% 
  select(CBSA, `Total Pop over 25 with Bachelor degree or higher`, `Total Pop over 25 with Science & Engineering or higher`, `Total POP Under 125 percent of poverty level`) %>% 
  left_join(full_set4, by = c("CBSA")) 
  
  

write.xlsx(full_set4, file = "outputs/navy_stem_analysis2.xlsx")

```

